<?php
namespace Xaamin\XmlToArray\Support;

use DOMText;
use DOMElement;
class Node
{
    /** @var string */
    private $name;

    /** @var string */
    private $tag;

    /** @var Collection */
    private $children;

    /** @var array<string, string> */
    private $attributes;

    /** @var UnboundedNodes */
    private $unbounded;

    /**
     * @param UnboundedNodes $unbounded
     */
    public function __construct(UnboundedNodes $unbounded = null)
    {
        $this->unbounded = $unbounded ? : new UnboundedNodes();
    }

    /**
     * @param \DOMElement $element
     * @param $parentIdentifier The parent key
     *
     * @return Node
     */
    public function parse(DOMElement $element, $parentIdentifier = '')
    {
        $this->name = $element->localName;
        $this->tag = $this->makeNodeTag($element, $parentIdentifier);
        $this->attributes = $this->makeAttributes($element);
        $this->children = $this->makeChildren($element);

        return $this;
    }

    /**
     * Set the unbounded node values
     *
     * @param UnboundedNodes The unbounded nodes values
     *
     * @return Node
     */
    public function withUnboundedNodes(UnboundedNodes $unbounded)
    {
        $this->unbounded = $unbounded;

        return $this;
    }

    /**
     * Get the unbounded nodes
     *
     * @return UnboundedNodes
     */
    public function getUnboundedNodes()
    {
        return $this->unbounded;
    }

    /**
     * Node name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Node tag
     *
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * Node attributes
     *
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Node children
     *
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param DOMElement $element
     *
     * @return array<string, string>
     */
    private function makeAttributes(DOMElement $element): array
    {
        /**
         * @var DOMNamedNodeMap<DOMAttr> $attributesMap
         */
        $attributesMap = $element->attributes;

        $attributes = [];

        foreach ($attributesMap as $attribute) {
            $attributes[$attribute->localName] = $attribute->value;
        }

        return $attributes;
    }

    /**
     * @param DOMElement $element
     *
     * @return Collection
     */
    private function makeChildren(DOMElement $element)
    {
        $children = new Collection($this->unbounded);

        foreach ($element->childNodes as $child) {
            if ($child instanceof DOMElement) {
                if ($child->childNodes->length === 1 && $child->childNodes[0] instanceof DOMText) {
                    $value = trim($child->nodeValue);

                    $key = null;

                    if ($child->attributes && $child->attributes->count() === 1) {
                        $key = $child->attributes[0]->nodeValue;
                    }

                    if ($value !== '') {
                        if (isset($this->attributes[$child->localName])) {
                            if (is_array($this->attributes[$child->localName])) {
                                if ($key) {
                                    $this->attributes[$child->localName][$key] = $value;
                                } else {
                                    $this->attributes[$child->localName][] = $value;
                                }
                            } else {
                                $this->attributes[$child->localName] = [
                                    $this->attributes[$child->localName],
                                    $value
                                ];
                            }
                        } else {
                            $this->attributes[$child->localName] = $key ? [$key => $value] : $value;
                        }
                    }
                }

                $children->add((new Node($this->unbounded))->parse($child, $this->tag));
            }
        }

        return $children;
    }

    /**
     * Make the node tag identifier
     *
     * @param DOMElement $element
     * @param string $parentIdentifier
     *
     * @return string
     */
    private function makeNodeTag(DOMElement $element, string $parentIdentifier): string
    {
        $separator = $this->unbounded->getSeparator();

        return ltrim($parentIdentifier . $separator . $element->localName, $separator);
    }

    /**
    * Get an item from an array using "dot" notation.
    *
    * @param string $key
    * @param mixed $default
    *
    * @return mixed
    */
    public function get($key = null, $default = null)
    {
        if (is_null($key)) {
            return $this->toArray();
        }

        if (isset($this->attributes[$key])) {
            return $this->attributes[$key];
        }

        $segments = explode('.', $key);
        $segment = array_shift($segments);

        if ($this->children->contains($segment)) {
            foreach ($this->children->all() as $node) {
                if ($node->getName() === $segment) {
                    return $node->get(implode('.', $segments) ? : null);
                }
            }
        }

        return $default;
    }

    /**
     * Converts to array the node
     *
     * @return array<string, string|array>
     */
    public function toArray(): array
    {
        return $this->attributes + $this->children->toArray();
    }
}

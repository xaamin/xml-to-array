<?php
namespace Xaamin\XmlToArray\Support;

use JsonException;
use LogicException;

class UnboundedNodesFromJsonFile
{
    /**
     * Unbounded nodes from xsd
     *
     * @var UnboundedNodesFromXsd
     */
    protected $factory;

    /**
     * Constructor
     *
     * @param UnboundedNodesFromXsd|null $factory
     */
    public function __construct(UnboundedNodesFromXsd $factory = null)
    {
        $this->factory = $factory ? : new UnboundedNodesFromXsd();
    }

    /**
     * Generate the unbounded nodes data
     *
     * @param string $asset
     * @param string $separator The tag name separator
     *
     * @return UnboundedNodes
     */
    public function make($asset, $separator = ' > ')
    {
        $contents = $asset;

        $file = strval(str_replace("\0", '', $asset));

        if (file_exists($file)) {
            $contents = file_get_contents($file);
        }

        if (!$contents) {
            throw new LogicException("Unable to open file $file");
        }

        return new UnboundedNodes($this->makeData($contents), $separator);
    }

    /**
     * @param string $contents
     * @return array<string>
     *
     * @throws JsonException|LogicException
     */
    public function makeData(string $contents)
    {
        $data = json_decode(strval($contents), true, 512, JSON_THROW_ON_ERROR);

        if (!is_array($data)) {
            throw new JsonException('Invalid JSON content, it\'s not parseable.');
        }

        foreach ($data as $index => $value) {
            if (!is_string($value)) {
                throw new LogicException("Invalid index $index, it must be a string.");
            }
        }

        return $data;
    }
}
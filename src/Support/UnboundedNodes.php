<?php
namespace Xaamin\XmlToArray\Support;

class UnboundedNodes
{
    /**
     * Data
     *
     * @var array
     */
    protected $data = [];

    /**
     * Prefixes to be ignored
     *
     * @var array
     */
    protected $ignores = [];

    /**
     * The tag name char separator
     *
     * @var string
     */
    protected $separator = ' > ';

    /**
     * Constructor
     *
     * @param array|null $data
     * @param string $separator The tag name char separator
     */
    public function __construct(array $data = null, $separator = ' > ')
    {
        !!$data && $this->setData($data);

        $this->separator($separator);
    }

    /**
     * Set the character used as tag separator
     *
     * @param string $char
     *
     * @return UnboundedNodes
     */
    public function separator($char)
    {
        $this->separator = $char;

        return $this;
    }

    /**
     * Prefixes array yo be ignored
     *
     * @param array<string> $keys
     *
     * @return UnboundedNodes
     */
    public function ignore(array $keys)
    {
        $callback = function ($value) {
            return trim($value) . $this->separator;
        };

        $this->ignores = array_map($callback, $keys);

        return $this;
    }

    /**
     * Set the unbounded nodes data
     *
     * @param array<string> $data
     *
     * @return UnboundedNodes
     */
    public function setData(array $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get the unbounded nodes data
     *
     * @return array<string>
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Get the tag name node separator
     *
     * @return string
     */
    public function getSeparator()
    {
        return $this->separator;
    }

    /**
     * Validates if the unbounded data contains a specific key
     *
     * @param string $key
     *
     * @return bool
     */
    public function contains($key)
    {
        $key = trim(str_replace($this->ignores, '', $key));

        return in_array($key, $this->data);
    }

}
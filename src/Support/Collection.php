<?php
namespace Xaamin\XmlToArray\Support;

use Xaamin\XmlToArray\Support\UnboundedNodes;

class Collection
{
    /**
     * Children array
     *
     * @var Node[]
     */
    private $children = [];

    /** @var UnboundedNodes */
    private $unbounded;

    private $keys = [];

    /**
     * @param UnboundedNodes $map
     */
    public function __construct(UnboundedNodes $map = null)
    {
        $this->unbounded = $map ? : new UnboundedNodes();
    }

    /**
     * @param Node $element
     *
     * @return Collection
     */
    public function add(Node $child)
    {
        $this->children[] = $child;

        if (!isset($this->keys[$child->getName()])) {
            $this->keys[$child->getName()] = 1;
        } else {
            $this->keys[$child->getName()]++;
        }

        return $this;
    }

    /**
     * Returns the child number
     *
     * @return int
     */
    public function count()
    {
        return count($this->children);
    }

    /**
     * @param Node $element
     *
     * @return boolean
     */
    public function hasMoreThanOneChild(Node $child): bool
    {
        return (isset($this->keys[$child->getName()]) && $this->keys[$child->getName()] > 1) || $this->unbounded->contains($child->getTag());
    }

    /**
     * Determines if the children contains the provided key
     *
     * @param string $key
     *
     * @return boolean
     */
    public function contains($key)
    {
        return isset($this->keys[$key]);
    }

    /**
     * Returns all children
     *
     * @return array
     */
    public function all()
    {
        return $this->children;
    }

    /**
     * Converts to array the node collection
     *
     * @return array<string, string|array>
     */
    public function toArray(): array
    {
        $children = [];

        foreach ($this->children as $item) {
            if ($this->hasMoreThanOneChild($item)) {
                $children[$item->getName()][] = $item->toArray();
            } else {
                $value = $item->toArray();

                $children[$item->getName()] = count($value) > 0 ? $value : '';
            }
        }

        return $children;
    }
}
<?php
namespace Xaamin\XmlToArray\Support;

use DOMXPath;
use DOMDocument;

class UnboundedNodesFromSchemas
{
    /**
     * he node names separator
     *
     * @var string
     */
    protected $separator = ' > ';

    /**
     * Unbounded nodes from xsd
     *
     * @var UnboundedNodesFromXsd
     */
    protected $factory;

    /**
     * Constructor
     *
     * @param UnboundedNodesFromXsd|null $factory
     */
    public function __construct(UnboundedNodesFromXsd $factory = null)
    {
        $this->factory = $factory ? : new UnboundedNodesFromXsd();
    }

    /**
     * Generate the unbounded nodes data
     *
     * @param string $asset
     *
     * @return UnboundedNodes
     */
    public function make($asset, $separator = ' > ')
    {
        $schemas = $this->getSchemas($asset);

        unset($asset);

        $unbounded = [];

        foreach ($schemas as $schema) {
            @list(, $location) = explode(' ', $schema);

            $nodes = $this->factory->make($location, $separator)->getData();

            $unbounded = !!$location ? array_merge($unbounded, $nodes) : $unbounded;
        }

        return new UnboundedNodes($unbounded, $separator);
    }

    /**
     * Get the schemas for an xml file
     *
     * @param string $xml
     *
     * @return array<string>
     */
    protected function getSchemas($xml)
    {
        $file = strval(str_replace("\0", '', $xml));

        $dom = new DOMDocument();

        if (file_exists($file)) {
            $dom->load($file);
        } else {
            $dom->loadXml($xml);
        }

        $xpath = new DOMXPath($dom);

        $schemas = [];

        $nodes = $xpath->query("//*[@xsi:schemaLocation]");

        foreach ($nodes as $node) {
            $schemaLocation = $node->getAttribute('xsi:schemaLocation');

            $segments = explode(' ', preg_replace('/\s+/', ' ', $schemaLocation));

            while (count($segments)) {
                $schema = array_shift($segments);
                $location = array_shift($segments);

                $schemaLocation = trim("{$schema} {$location}");

                if ($schemaLocation && !in_array($schemaLocation, $schemas)) {
                    $schemas[] = $schemaLocation;
                }
            }
        }

        unset($dom, $xpath, $file, $xml);

        return $schemas;
    }

    /**
     * Magic method calls
     *
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return call_user_func([$this->factory, $name], $arguments);
    }

}
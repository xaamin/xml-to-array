<?php
namespace Xaamin\XmlToArray\Support;

use DOMXPath;
use DOMElement;
use DOMDocument;
use DOMNodeList;

class UnboundedNodesFromXsd
{
    /**
     * Internal cache path to store downloaded assets
     *
     * @var string
     */
    protected $cachePath;

    /**
     * Assets path
     *
     * @var string
     */
    protected $assetsPath;

    /**
     * Constructor
     *
     * @param string $cachePath
     * @param string $assetsPath
     */
    public function __construct($cachePath = null, $assetsPath = null)
    {
        !!$cachePath && $this->setCachePath($cachePath);
        !!$assetsPath && $this->setAssetsPath($assetsPath);
    }

    /**
     * Sets the cache path
     *
     * @param string $path
     *
     * @return UnboundedNodesFromXsd
     */
    public function setCachePath($path)
    {
        $this->cachePath = rtrim($path, DIRECTORY_SEPARATOR);

        return $this;
    }

    /**
     * Gets the cache path
     *
     * @return string
     */
    public function getCachePath()
    {
        return $this->cachePath ? :  sys_get_temp_dir();
    }

    /**
     * Sets the assets path
     *
     * @param string $path
     *
     * @return UnboundedNodesFromXsd
     */
    public function setAssetsPath($path)
    {
        $this->assetsPath = rtrim($path, DIRECTORY_SEPARATOR);

        return $this;
    }

    /**
     * Gets the assets path
     *
     * @return string
     */
    public function getAssetsPath()
    {
        return $this->assetsPath ? :  sys_get_temp_dir();
    }

    /**
     * Generates the unbounded nodes from the given asset
     *
     * @param string $asset
     * @param string $separator The char used as tag separator
     *
     * @return UnboundedNodes
     */
    public function make($asset, $separator = ' > ')
    {
        $contents = null;
        $filename = md5($asset);

        $asset = $this->getAssetPath($asset);

        $cache = $this->getCachePath() . DIRECTORY_SEPARATOR . $filename;

        if (!file_exists($cache)) {
            $contents = file_get_contents($asset);

            file_put_contents($cache, $contents);
        } else {
            $contents = file_get_contents($cache);
        }

        $document = new DOMDocument();
        $document->loadXML($contents);

        // Min occurs > 1 |  Max occurs > 1 | Unbounded

        $elements = $this->getPathUsingXPathQuery(
            $document,
            '//x:element[@minOccurs > 1 or @maxOccurs > 1 or @maxOccurs = "unbounded"]',
            $separator
        );
        $secuences = $this->getPathUsingXPathQuery(
            $document,
            '//x:sequence[@minOccurs > 1 or @maxOccurs > 1 or @maxOccurs = "unbounded"]/x:element',
            $separator
        );
        $choices = $this->getPathUsingXPathQuery(
            $document,
            '//x:choice[@minOccurs > 1 or @maxOccurs > 1 or @maxOccurs = "unbounded"]/x:element',
            $separator
        );

        $nodes = array_merge($elements, $secuences, $choices);

        return new UnboundedNodes($nodes, $separator);
    }

    /**
     * Get the asset path for a given asset
     *
     * @param string $asset
     *
     * @return string
     */
    protected function getAssetPath($asset)
    {
        $local = parse_url($asset, PHP_URL_HOST) . DIRECTORY_SEPARATOR . trim(parse_url($asset, PHP_URL_PATH), DIRECTORY_SEPARATOR);

        if (DIRECTORY_SEPARATOR !== '/') {
            $local = str_replace('/', DIRECTORY_SEPARATOR, $local);
        }

        if (!!$this->assetsPath) {
            $location = rtrim($this->assetsPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $local;
            $altLocation = string_to_lowercase($location);

            if (file_exists($location)) {
                $asset = $location;
            } else if ($location !== $altLocation && file_exists($altLocation)) {
                $asset = $altLocation;
            }
        }

        return $asset;
    }

    /**
     * @param DOMDocument $document
     * @param string $query
     * @param string $separator
     *
     * @return string[]
     */
    private function getPathUsingXPathQuery(DOMDocument $document, string $query, $separator): array
    {
        $paths = [];
        $xpath = new DOMXPath($document);
        $xpath->registerNamespace('x', 'http://www.w3.org/2001/XMLSchema');

        $nodes = $xpath->query($query) ?: new DOMNodeList();

        foreach ($nodes as $node) {
            if ($node instanceof DOMElement) {
                $paths[] = $this->obtainPathForElement($node, $separator);
            }
        }

        return $paths;
    }

    /**
     * Get the path/tag names for a given element
     *
     * @param DOMElement $xsElement
     * @param string $separator
     *
     * @return string
     */
    private function obtainPathForElement(DOMElement $xsElement, $separator)
    {
        $pathItems = [];

        while (null !== $xsElement) {
            $pathItems[] = $xsElement->getAttribute('name');

            $xsElement = $this->findParentElement($xsElement);
        }

        return implode($separator, array_reverse($pathItems));
    }

    /**
     * Find the parent element for a given node
     *
     * @param DOMElement $node
     *
     * @return DOMElement|null
     */
    private function findParentElement(DOMElement $node): ?DOMElement
    {
        $node = $node->parentNode;

        while ($node !== null) {
            $isNodeElement = $node->localName !== 'element';
            $isW3XmlSchema = $node->namespaceURI !== 'http://www.w3.org/2001/XMLSchema';

            if ($isNodeElement || $isW3XmlSchema) {
                $node = $node->parentNode;

                continue;
            }

            return $node;
        }

        return null;
    }

}

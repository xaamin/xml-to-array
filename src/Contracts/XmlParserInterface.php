<?php
namespace Xaamin\XmlToArray\Contracts;

interface XmlParserInterface
{
    /**
     * Load a given XML to process as array
     *
     * @param string $xml
     *
     * @return void
     */
    public function load($xml);

    /**
     * Array representation for the xml
     *
     * @return array
     */
    public function toArray();

    /**
     * JSON representation for the xml
     *
     * @return array
     */
    public function toJson();

    /**
     * String representation for the xml
     *
     * @return array
     */
    public function __toString();
}
<?php
namespace Xaamin\XmlToArray;

use DOMDocument;
use Xaamin\XmlToArray\Support\Node;
use Xaamin\XmlToArray\Support\UnboundedNodes;

class XmlParser
{
    /** @var \Xaamin\XmlToArray\Support\Node */
    private $parser;

    /**
     * DOM Document, XML holder
     *
     * @var \DOMDocument
     */
    protected $document;

    protected $parsed = false;

    /**
     * @param \Xaamin\XmlToArray\Support\Node $parser
     */
    public function __construct(Node $parser = null)
    {
        $this->parser = $parser ? : new Node();
    }

    /**
     * Set the unbounded node values
     *
     * @param \Xaamin\XmlToArray\Support\UnboundedNodes The unbounded nodes factory
     *
     * @return \Xaamin\XmlToArray\Support\XmlParser
     */
    public function withUnboundedNodes(UnboundedNodes $unbounded)
    {
        $this->parser->withUnboundedNodes($unbounded);

        return $this;
    }

    /**
     * Load an xml into the current parsing process
     *
     * @param string $xml
     *
     * @return \Xaamin\XmlToArray\XmlParser
     */
    public function load($xml)
    {
        $file = is_string($xml) ? strval(str_replace("\0", '', $xml)) : null;

        $document = new DOMDocument();

        if ($xml instanceof DOMDocument) {
            $document = $xml;
        } else if (!!$file && file_exists($file)) {
            $document->load($file);
        } else {
            $document->loadXML($xml);
        }

        $this->document = $document;

        unset($xml);

        $this->reset();

        return $this;
    }

    /**
     * Load an xml into the current parsing process
     *
     * @param string $xml
     * @return \Xaamin\XmlToArray\XmlParser
     */
    public static function make($xml, Node $parser = null)
    {
        return (new static($parser))->load($xml);
    }

    /**
     * Parses the xml
     *
     * @return \Xaamin\XmlToArray\Support\Node
     */
    public function parse()
    {
        $this->parsed = true;

        return $this->parser->parse($this->document->documentElement);
    }

    /**
     * Resets the parsing proces
     *
     * @return \Xaamin\XmlToArray\XmlParser
     */
    public function reset()
    {
        $this->parsed = false;

        return $this;
    }

    public function __call($name, $arguments)
    {
        if (in_array($name, ['get', 'toArray']) && !$this->parsed) {
            $this->parse();
        }

        return call_user_func_array([$this->parser, $name], $arguments);
    }
}